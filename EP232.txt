
Documentation for:
			EP232.EXEC.

Written by:
		 Joseph L Moschini.
                jmos@iprimus.com.au
Usage:
	To run Diamond Systems EP232.

Associated files:
			EP232.C the source file under Hitech C.
			EP232fk.shell is fkey assignment.
	
Purpose:
	To read and program EPROMs from 2716 to 27512

Commands:
		The commands are menu driven, one key stroke 
		execution.

		** Initial set up commands ** 
		A is to enter filename.
		B is to enter number of bytes to read or write
		  from/to EPROM.
		E is for file offset this is to avoid the first
		  section of file for spliting of roms.
		S is for serial selection sa: or sb:

		** Executing commands **
		C is for comparing file with rom.
		D is for 1616o/s commands.
		F is for ffhex test in the EPROM.
		R is to read EPROM into file.
		W is to program EPROM from file.

The Rules:
		The main rule is to set the serial selection 
		before inserting EPROMs.

Notes:
	This is an extract from the manual for the EP232.

	The programer is controlled entirely by the RS232 lines;
			DTR and RX.

	DTR controls the state of the programmer. DTR low sets
	the programmer in the read mode. DTR high selects the
	program mode. DTR going from low to high (read to program),
	is also used to reset the address counters to 000. 
	Before any operation, this line should be strobed to 
	reset the address. The strobe interval should not be 
	longer than about a milli second, 100 - 200 micro seconds
	is plenty, this prevents the programmer entering the 
	program mode as a delay is incorporated in the relay driver
	circuit.

	After the address counters have been reset, sending a byte to
	the unit will cause it to return the byte appearing at the
	EPROM output in aprox 1ms. It will then increment the address
	counter. In the program mode, the byte is not returned and 
	the address counters are not incremented until after the
	program pulse interval. The RX LED flashes for ever character
	received from the computer. In read mode this shows as a
	continuous glow and in program mode individual flashes are
	discernable for the longer pulse durations.

	More information on the EP232 a service manual with circuit
	diagrams is available from Diamond Systems for $40.00.

	************  Problems installing EP232 ************

	This text on the problems of installing the ep232
	on the Applix 1616.

	My first problem was RS232 DTR line.
	The DTR would go high after a GETC Syscall. This causes
	the EP232 to reset the address counters and goes into
	program mode.

	Solved this problem by debuging part of the 1616 rom and
	found two ram locations. These are used as shadow registers
	are loaded into write register 5 of the SCC.

		The SCC shadow write register 5 for version 2.4a.
			$2882: $ffea     this is for serial a.
			$2884: $ffea     this is for serial b.

	To turn off the DTR we put a value of ff6a into any of the
	above locations.

	Now there is a new version 3.0b which I have debuged and
	found a new location for shadow write register 5 for SCC.

			$2118: $ffea	this is for serial a.
			$211a: $ffea    this is for serial b.

	So there is no guaranty of DTR having the same shadow ram
	location in any up and come versions of 1616o/s.

	One way to fix this problem is to make a standard shadow register
	for the users to use.

				OR

	Make a system call like.

	dtr(cdn,mode)	Set DTR with mode at stream cdn. 
	d0:	code
	d1:	cdn:		Character device number.
	d2:	mode:		0: DTR low 1:DTR high 2:determine current mode.
	Return:			Current mode.

			Problem number 2.

	Yes another problem. This is with the baud rate setings.
	The ep232 needs	to run at 38400 baud for roms from 27128 to 27512.
	The current settings is 9600 baud a little bit slow for big PROMs.
	At 38400 baud the serial is sending garbage to the EP232, But the
	1616 SCC reads from the EP232 ok. This may be due to the SCC handling
	errors the EP232 uses a simple UART IC whitch probably does not fix
	errors.

				OR

	The other factor could be that the clock speed for the baud rate
	on the Applix is 3.75 Mhz. making the baud rate slightly faster.

	Plus the EP232 clock speed is a fraction slower.

	So one idea is to make a sub pcb that plugs into the 1616 option
	shorting pins with the correct clock frequency whitch is 3.6864 Mhz.

	A project for the future. ???


	This for ver 1.5 EP232. Baud rate problem solved on Aug 1988,
	I installed 3.6864 Mhz. crystal and an inverter I.C. on sub
	pcb, pluged into the the serial option links. Now the EP232
	works at 38400 baud.

